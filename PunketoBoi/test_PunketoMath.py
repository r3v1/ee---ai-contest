#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest

from PunketoExceptions import MapaMalFormado
from PunketoMath import map2graph, shortest_path


class TestPunketoMath(unittest.TestCase):
    def setUp(self):
        self.normal_map = [[0, 0, 0, 0, 0],
                           [0, 1, 0, 1, 0],
                           [0, 1, 1, 0, 0],
                           [0, 1, 0, 1, 0],
                           [0, 0, 1, 0, 0],
                           [0, 0, 0, 0, 0]]

        self.tuple_map = ((0, 0, 0, 0, 0),
                          (0, 1, 0, 1, 0),
                          (0, 1, 1, 0, 0),
                          (0, 1, 0, 1, 0),
                          (0, 0, 1, 0, 0),
                          (0, 0, 0, 0, 0))

        self.empty_map = [[0, 0, 0],
                          [0, 0, 0]]

        self.no_ways_map = [[0, 0, 0, 0],
                            [0, 1, 0, 0],
                            [0, 0, 0, 1],
                            [1, 0, 0, 0]]

        self.horizontal_empty = [[0, 0, 0, 0, 0, 0]]

        self.no_ways_horizontal_map = [[0, 1, 0, 1, 0]]

        self.mapa_horizontal = [[0, 1, 1, 0, 0]]

        self.mapa_error = [0, 1, 1, 0, 0]

    def test_map2graph(self):
        # Mapa normal
        self.assertEqual({
            (1, 3): [((1, -1), (2, 2))], (3, 3): [((-1, -1), (2, 2)), ((1, -1), (4, 2))],
            (3, 1): [((1, 1), (4, 2)), ((-1, 1), (2, 2)), ((-1, 0), (2, 1))],
            (2, 1): [((1, 0), (3, 1)), ((0, 1), (2, 2)), ((-1, 0), (1, 1))],
            (2, 2): [((1, -1), (3, 1)), ((0, -1), (2, 1)), ((1, 1), (3, 3)), ((-1, 1), (1, 3)), ((-1, -1), (1, 1))],
            (4, 2): [((-1, -1), (3, 1)), ((-1, 1), (3, 3))],
            (1, 1): [((1, 0), (2, 1)), ((1, 1), (2, 2))]
        }, map2graph(self.normal_map))

        # Mapa sin casillas
        self.assertEqual({}, map2graph(self.empty_map))

        # Mapa sin caminos
        self.assertEqual({
            (1, 1): [],
            (2, 3): [],
            (3, 0): []
        }, map2graph(self.no_ways_map))

        # Mapa horizontal vacío
        self.assertEqual({}, map2graph(self.horizontal_empty))

        # Mapa horizontal sin caminos
        self.assertEqual({
            (0, 1): [],
            (0, 3): []
        }, map2graph(self.no_ways_horizontal_map))

        # Mapa horizontal con caminos
        self.assertEqual({
            (0, 1): [((0, 1), (0, 2))],
            (0, 2): [((0, -1), (0, 1))]
        }, map2graph(self.mapa_horizontal))

        # Mapa error
        self.assertRaises(MapaMalFormado, map2graph, self.mapa_error)

        # Mapa con tuplas
        self.assertEqual({
            (1, 3): [((1, -1), (2, 2))], (3, 3): [((-1, -1), (2, 2)), ((1, -1), (4, 2))],
            (3, 1): [((1, 1), (4, 2)), ((-1, 1), (2, 2)), ((-1, 0), (2, 1))],
            (2, 1): [((1, 0), (3, 1)), ((0, 1), (2, 2)), ((-1, 0), (1, 1))],
            (2, 2): [((1, -1), (3, 1)), ((0, -1), (2, 1)), ((1, 1), (3, 3)), ((-1, 1), (1, 3)), ((-1, -1), (1, 1))],
            (4, 2): [((-1, -1), (3, 1)), ((-1, 1), (3, 3))],
            (1, 1): [((1, 0), (2, 1)), ((1, 1), (2, 2))]
        }, map2graph(self.tuple_map))

    def test_shortest_path(self):
        # Grafo incorrecto
        self.assertRaises(MapaMalFormado, shortest_path, (1, 1), (2, 2), self.normal_map)

        # Camino único
        self.assertEqual(shortest_path((1, 1), (2, 2), map2graph(self.normal_map)), ([(1, 1)], 1))
        self.assertEqual(shortest_path((1, 1), (1, 3), map2graph(self.normal_map)), ([(1, 1), (-1, 1)], 2))

        # Camino múltiple
        self.assertIn(shortest_path((1, 1), (3, 1), map2graph(self.normal_map)), [
            ([(1, 1), (1, -1)], 2),
            ([(1, 0), (1, 0)], 2)
        ])

        # No hay camino
        self.assertEqual(shortest_path((1, 1), (4, 3), map2graph(self.normal_map)), (None, None))

        # No hay camino (ya he llegado)
        self.assertEqual(shortest_path((1, 1), (1, 1), map2graph(self.normal_map)), ([], 0))


if __name__ == '__main__':
    unittest.main()
