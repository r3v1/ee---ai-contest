# PunketoBoi 1.0
Bot de mofa para jugar al *Lighthouse Contest* de la Euskal Encounter 2019.

## Requerimientos
No se necesita más que Python 3.5 o superior (supongo).

## Uso
Para usarlo hay que pasarlo como parámetro al motor del juego, que esta escrito en Python 2.7:

```python2 engine/game.py <mapa> "<bot 1>" ... "<bot n>"```

siendo ```<mapa>``` la ruta a uno de los mapas, por ejemplo, ```maps/island.txt``` y ```"<bot n>"``` el bot en cuestión, en este caso con Python3, ```"python3 PunketoBoi/PunketoBoi.py"```.

Se supone que funciona en Arch Linux 5.2.1.

## Autor
D. Revillas - drevillas002@ikasle.ehu.eus

**Software prestado a O. Trigueros para la presentación en la EE 2019**
