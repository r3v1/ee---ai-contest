#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import sys
import random

from PunketoMath import map2graph, shortest_path, colinear, intersect
from interface import Interface


class PunketoBoi:
    NAME = "PunketoBoi"

    # Porcentage de energía mínima a conservar [0, 1]
    e_minima = 0.15

    def __init__(self, init_state):
        # ==========================================================================
        #                  Inicialización del bot al iniciar el juego
        # ==========================================================================
        self.player_num = init_state["player_num"]
        self.player_count = init_state["player_count"]
        self.init_pos = init_state["position"]
        self.map = init_state["map"]
        self.lighthouses = map(tuple, init_state["lighthouses"])

        self.bot_class = PunketoBoi
        self.bot = None

        self.graph = map2graph(self.map)
        self.ultimo_movimiento = None
        self.conexiones = []

        # Seguimiento de ruta
        self.siguiendo_ruta = False
        self.ruta = []

        self.faros_poseidos = []
        self.faros_poseidos_prioritarios = []
        self.faros_ajenos = []
        self.faros = {}

        self.mi_energia = 0
        self.mi_posicion = None
        self.mi_posicion_anterior = None
        self.turnos_sin_moverme = 0

        self.me_he_alejado = False
        self.alejandome = False

    # ==========================================================================
    #                           Comportamiento del bot
    #           Métodos a implementar / sobreescribir (opcionalmente)
    # ==========================================================================

    def success(self):
        """Éxito: llamado cuando la jugada previa es válida."""
        pass

    def error(self, message, last_move):
        """Error: llamado cuando la jugada previa no es válida."""
        self.log("Recibido error: %s", message)
        self.log("Jugada previa: %r", last_move)

    def play(self, state):
        """
        Jugada a realizar.

        :param state: diccionario con el estado actual del bot
        :return: json con la jugada
        """
        # ==========================================================================
        #                       Actualización del estado del bot
        # ==========================================================================

        # Actualiza el estado del bot
        self.actualizar_estado_bot(state)

        # ==========================================================================
        #                              Selección de jugada
        # ==========================================================================

        def jugada_es_mio():
            """
            Expansión del árbol de decisión si el faro al que he llegado es mío
            :return:
            """
            if self.mi_posicion in self.faros_poseidos:
                # Si el faro al que he llegado es mío
                if self.conserva_clave(self.mi_posicion):
                    # Si conserva la clave
                    faro = self.faro_para_cerrar_triangulo()
                    if faro and self.turnos_sin_moverme < 2:
                        # Si existe un faro para cerrar el triángulo
                        if self.faros[faro]['have_key']:
                            # Si se puede conectar, conectarlo
                            return self.conectar((faro[1], faro[0]))
                        else:
                            # Si no, se tiene la clave, ir a ese faro y recuperar la clave
                            self.buscar_ruta(faro_concreto=faro)
                            return dar_paso()
                    else:
                        # Si no existe un faro para cerrar el triángulo
                        faro_destino, ruta, distancia = self.obtener_faro_conectable()
                        if faro_destino and self.turnos_sin_moverme < 2:
                            return self.conectar((faro_destino[1], faro_destino[0]))
                        else:
                            # Dar el primer paso de la ruta
                            self.buscar_ruta()
                            return dar_paso()
                else:
                    # Si el faro no conserva la clave
                    faro_destino, ruta, distancia = self.obtener_faro_conectable()
                    if faro_destino and self.turnos_sin_moverme < 2:
                        return self.conectar((faro_destino[1], faro_destino[0]))
                    else:
                        # Dar el primer paso de la ruta
                        self.ruta = ruta
                        self.siguiendo_ruta = True
                        return dar_paso()

            else:
                # Si el faro al que he llegado no es mío
                if self.mi_energia * (1 - self.e_minima) > self.faros[self.mi_posicion]['energy']:
                    # Si conservo algo de energía y es suficiente para hacerlo mío
                    if self.turnos_sin_moverme < 3:
                        # Comprobar que no me quedo 'luchando' por un faro con otro bot indefinidamente
                        return self.atacar()
                    else:
                        # Si llevo un rato luchando por el mismo faro, me piro
                        self.buscar_ruta()
                        return dar_paso()
                else:
                    # Si no tengo energía
                    self.buscar_ruta()
                    return dar_paso()

        def dar_paso():
            """
            Ejecuta un paso de la ruta.
            :return: moviemiento
            """
            try:
                movimiento = self.ruta.pop(0)
            except IndexError:
                # En caso de no haber movimientos disponibles, buscar una nueva ruta
                self.buscar_ruta()
                return dar_paso()
            return self._move(movimiento[1], movimiento[0])

        # Jugada por defecto
        jugada = self._nop()

        # TÁCTICA: La primera jugada será alejarme lo máximo posible
        if self.me_he_alejado:
            if self.siguiendo_ruta:
                # Si estoy siguiendo una ruta
                if self.mi_posicion in self.faros:
                    # Si he llegado a un faro del mapa
                    if not self.alejandome:
                        self.siguiendo_ruta = False

                    jugada = jugada_es_mio()
                else:
                    # Si aún no he llegado, dar un paso
                    jugada = dar_paso()

            else:
                # Si no estoy siguiendo una ruta
                if self.mi_posicion in self.faros:
                    # Si no estoy siguiendo una ruta y he llegado a un faro del mapa
                    jugada = jugada_es_mio()
                else:
                    # Si no estoy siguiendo una ruta y no he llegado a un faro del mapa
                    self.buscar_ruta()
                    jugada = dar_paso()
        else:
            self.alejarme()
            jugada = dar_paso()
            self.me_he_alejado = True

        # Comprobar que no me quedo parado en una casilla
        if self.mi_posicion == self.mi_posicion_anterior:
            self.turnos_sin_moverme += 1
        else:
            self.turnos_sin_moverme = 0

        self.mi_posicion_anterior = self.mi_posicion

        return jugada

    # ==========================================================================
    #                                  Utilidades
    #                   No es necesario sobreescribir estos métodos.
    # ==========================================================================

    def log(self, message, *args):
        """Mostrar mensaje de registro por stderr"""
        print("[%s] %s" % (self.NAME, (message % args)), file=sys.stderr)

    # ==========================================================================
    #                               Jugadas posibles
    #                 No es necesario sobreescribir estos métodos.
    # ==========================================================================

    @staticmethod
    def _nop():
        """Pasar el turno"""
        return {
            "command": "pass",
        }

    @staticmethod
    def _move(x, y):
        """Mover a una casilla adyacente

        x: delta x (0, -1, 1)
        y: delta y (0, -1, 1)
        """
        return {
            "command": "move",
            "x": x,
            "y": y
        }

    @staticmethod
    def _attack(energy):
        """Atacar a un faro

        energy: energía (entero positivo)
        """
        return {
            "command": "attack",
            "energy": energy
        }

    @staticmethod
    def _connect(destination):
        """Conectar a un faro remoto

        destination: tupla o lista (x,y): coordenadas del faro remoto
        """
        return {
            "command": "connect",
            "destination": destination
        }

    # ==========================================================================
    #                                Funcionalidades
    # ==========================================================================

    def actualizar_estado_bot(self, state):
        """
        Actualiza el estado el bot, como su posición, su energía o los faro poseídos.
        :param state: diccionario con la información del bot
        """
        self.mi_posicion = (state['position'][1], state['position'][0])
        self.mi_energia = state['energy']

        # Posiciones de los faros
        self.faros = dict(((lh["position"][1], lh["position"][0]), lh) for lh in state["lighthouses"])

        # Conexiones, faros ajenos y faros en posesión
        self.faros_poseidos = []
        self.faros_ajenos = []
        self.conexiones = []
        for faro in self.faros.keys():
            if self.faros[faro]['owner'] == self.player_num:
                self.faros_poseidos.append(faro)
                for conexion in self.faros[faro]['connections']:
                    conexion = (conexion[1], conexion[0])
                    if not (faro, conexion) in self.conexiones and not (conexion, faro) in self.conexiones:
                        self.conexiones.append((faro, conexion))
            else:
                self.faros_ajenos.append(faro)

        # Ordenar la lista de los faros poseídos según la energía que les quede
        self.faros_poseidos_prioritarios = self.faros_poseidos[:]
        self.faros_poseidos_prioritarios.sort(key=lambda x: self.faros[x]['energy'])

    def buscar_ruta(self, faro_concreto=None):
        """
        Define una ruta al bot para que la siga hasta que llegue a ella. En caso
        de proporcionar un faro concreto, se devuelve la ruta hacia ese faro.
        """
        ruta = []
        self.alejandome = False

        if not faro_concreto:
            # Escoger una ruta
            posibles_rutas = self.faros_mas_cercanos()
            rutas = self.analizar_rutas(posibles_rutas)

            # Escoger una ruta aleatoriamente
            (ruta, distancia), _ = random.choice(random.choice(rutas))
        else:
            # Ruta hacia faro concreto
            ruta, distancia = shortest_path(self.mi_posicion, faro_concreto, self.graph)

        # Establecer la ruta
        self.siguiendo_ruta = True
        self.ruta = ruta

    def analizar_rutas(self, rutas, lim_inf=0, lim_sup=float('inf'), num_distancias=2):
        """
        Dadas unas rutas en formato:
            [
                ( (ruta, distancia), faro_destino ),
            ]

        filtra las rutas más cortas.

        :param rutas: lista de tuplas con las rutas
        :param lim_inf: filtrar las rutas con una mínima distancia
        :param lim_sup: filtrar las rutas con una máxima distancia
        :param num_distancias: número de rutas con la misma distancia
        """

        d_corta = float('inf')
        lista = []
        for ((ruta, distancia), faro) in rutas:
            if num_distancias > len(lista):
                if lim_inf < distancia < lim_sup:
                    if len(lista) == 0 or d_corta != distancia:
                        d_corta = distancia
                        lista.append([((ruta, distancia), faro)])
                    else:
                        lista[len(lista)-1].append(((ruta, distancia), faro))
            else:
                break

        return lista

    def faros_mas_cercanos(self, en_posesion=False, reverse=False):
        """
        Devuelve una lista de las rutas a los faros más cercanos desde la
        posición actual y su distancia.

        :param en_posesion: es posible indicar que la lista devuelta contenga sólo faros
        que posea
        :param filtro_energia: es posible filtrar los resultados y devolver los faros que
        son factibles atacar. Es posible que nos dirijamos a un faro que tenga 5000 de energía
        cuando nosotros no tenemos ni 100, para no perder turnos a lo tonto.
        :param reverse: ornde inverso, más alejado primero
        """
        lista = []

        if en_posesion:
            lista_a_buscar = self.faros_poseidos
        else:
            lista_a_buscar = self.faros_ajenos

        for faro in lista_a_buscar:
            ruta, distancia = shortest_path(self.mi_posicion, faro, self.graph)
            if self.faros[faro]['energy'] - 10 * distancia < self.mi_energia:
                # Añadir únicamente el faro que pueda atacar cuando llegue allí
                lista.append(((ruta, distancia), faro))

        # Ordenar según la distancia al faro ascendentemente
        lista.sort(key=lambda x: x[0][1], reverse=reverse)

        return lista

    def atacar(self):
        """Atacar a un faro"""
        return self._attack(int(self.mi_energia * (1 - self.e_minima)))

    def conectar(self, faro_destino):
        """Conecta el faro en el que estoy con el faro de destino"""
        return self._connect(faro_destino)

    def cerrar_triangulo(self):
        """
        Definir una ruta hacia el faro con el que se pueda cerrar un triángulo.
        El faro tiene que estar en mi posesión y tiene que ser conectable.
        """
        # Escoger un faro para cerrar el triángulo
        faro_potencial = self.faro_para_cerrar_triangulo()

        if faro_potencial:
            # Si existe un faro potencial

            # Establecer la ruta
            self.ruta = shortest_path(self.mi_posicion, faro_potencial, self.graph)
            self.siguiendo_ruta = True
            return True
        else:
            self.siguiendo_ruta = False
            self.ruta = []
            return False

    def faro_para_cerrar_triangulo(self):
        """
        Devuelve un faro con el que se pueda cerrar el triángulo, dada mi posición.
        :return: faro en forma de tupla
        """
        faros_destino = []

        for faro_destino in self.faros_poseidos:
            if self.mi_posicion != faro_destino:
                # Si no estoy en el faro de destino
                if (self.mi_posicion, faro_destino) not in self.conexiones and (
                        faro_destino, self.mi_posicion) not in self.conexiones:
                    # Si no esta hecha ya la conexión
                    if not self.interseca_faro(faro_destino):
                        # Comprobar que la conexión no interseca otro faro
                        if not self.interseca_conexion(faro_destino):
                            # Comprobar que no interseca otra conexión hecha
                            _, distancia = shortest_path(self.mi_posicion, faro_destino, self.graph)
                            faros_destino.append((faro_destino, distancia))
        if faros_destino:
            faros_destino.sort(key=lambda x: x[1])
            return faros_destino[0][0]

        return None

    def es_conectable(self, faro_destino):
        """
        Devuelve True si se puede crear una conexión desde mi posición hasta el
        faro de destino dado. Tiene que cumplir la siguientes restricciones:

            - No conectar si el destino no es un faro
            - No conectar si el faro de destino no es de mi posesión
            - No conectar consigo mismo
            - No conectar si no tengo la clave de destino
            - No conectar si ya existe la conexión entre ese faro y mi posición
            - No conectar si la conexión interseca con otras de mis conexiones
            - No conectar si la conexión interseca un faro

        :param faro_destino: tupla indicando la posición del faro de destino
        :return: True si se puede conectar, False en caso contrario
        """

        if faro_destino in self.faros:
            # Si realmente es un faro
            if self.mi_posicion in self.faros_poseidos:
                # Si es un faro que poseo
                if self.mi_posicion != faro_destino:
                    # Si no estoy en el faro de destino
                    if self.faros[faro_destino]['have_key']:
                        # Si tengo la clave de destino
                        if (self.mi_posicion, faro_destino) not in self.conexiones and (
                                faro_destino, self.mi_posicion) not in self.conexiones:
                            # Si no esta hecha ya la conexión
                            if not self.interseca_faro(faro_destino):
                                # Comprobar que la conexión no interseca otro faro
                                if not self.interseca_conexion(faro_destino):
                                    # Comprobar que no interseca otra conexión hecha
                                    return True
        return False

    def interseca_faro(self, faro_destino):
        """
        TODO: Parece que no se da cuenta bien de la intersección de los faros
        Comprueba que la línea que se trace desde mi posición al faro de destino
        no interseca con ningún otro faro
        """
        x0, x1 = sorted((self.mi_posicion[0], faro_destino[0]))
        y0, y1 = sorted((self.mi_posicion[1], faro_destino[1]))
        for lh in self.lighthouses:
            if (x0 <= lh[0] <= x1 and y0 <= lh[1] <= y1 and lh not in (self.mi_posicion, faro_destino) and
                    colinear(self.mi_posicion, faro_destino, lh)):
                return True

        return False

    def interseca_conexion(self, faro_destino):
        """
        Comprueba si la conexión desde mi posición a la del faro interseca
        otra conexión existente.
        """
        for c in self.conexiones:
            if intersect(tuple(c), (self.mi_posicion, faro_destino)):
                return True

        return False

    def conserva_clave(self, faro):
        """
        Devuelve True si el faro aún se conserva la clave de conexión
        :param faro: tupla idnicando la posiciónn del faro
        :return: True o False
        """

        return self.faros[faro]['have_key']

    def obtener_faro_conectable(self):
        """
        Devuelve el faro en posesión más alejado que se puede conectar desde mi posición.

        :return tupla indicando la posición faro
        """

        # Comprobar que estoy en un faro de mi posesión
        if self.mi_posicion in self.faros_poseidos:
            # Intentamos coger el faro más alejado de nosotros para lograr la recta más larga

            for (ruta, distancia), faro_destino in self.faros_mas_cercanos(en_posesion=True)[::-1]:
                if self.es_conectable(faro_destino) and self.conserva_clave(faro_destino):
                    return faro_destino, ruta, distancia

        return None, None, None

    def alejarme(self):
        """
        TÁCTICA: Busca el faro más alejado desde mi posición y va hasta allí.
        """

        faros_mas_alejados = self.faros_mas_cercanos(en_posesion=False, reverse=True)
        rutas = self.analizar_rutas(faros_mas_alejados, num_distancias=2)

        # Escoger una ruta aleatoriamente
        (ruta, distancia), _ = random.choice(random.choice(rutas))

        self.ruta = ruta
        self.siguiendo_ruta = True

        # Evita que se acabe la ruta si encuentro un faro por el camino
        self.alejandome = True


def prueba():
    init_state = json.loads(
        '{"player_num": 0, "position": [8, 10], "player_count": 5, "lighthouses": [[5, 9], [13, 13], [5, 13], [9, 9], [13, 17], [5, 1], [9, 1], [5, 5], [1, 5], [9, 17], [5, 17], [1, 1], [17, 5], [9, 13], [1, 13], [17, 1], [1, 17], [1, 9], [9, 5], [17, 13], [13, 1], [17, 9], [13, 5], [13, 9], [17, 17]], "map": [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0], [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0], [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0], [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0], [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0], [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0], [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0], [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0], [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0], [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0], [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0], [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0], [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0], [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0], [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0], [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0], [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]}'
    )

    bot = PunketoBoi(init_state)

    with open("../states.txt", "r") as f:
        estados = f.read().split("\n")[:-1]

    i = 1
    for state in estados:
        state = eval(state)
        ronda = "Ronda {0}".format(i)
        # print ronda
        if i == len(estados) or i ==135:
            p = i
        jugada = bot.play(state)

        i += 1


if __name__ == "__main__":
    prueba()

    iface = Interface(PunketoBoi)
    iface.run()
