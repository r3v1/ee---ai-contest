#!/usr/bin/python3
# -*- coding: utf-8 -*-

from collections import deque

from PunketoExceptions import MapaMalFormado


def map2graph(mapa):
    """
    Adaptación de: http://bryukh.com/labyrinth-algorithms/

    Dada un mapa en forma de matriz de 0s y 1s, devuelve el grafo que representa
    la conexión de las casillas por las que haya camino (1). 0 indicaría que
    existe un muro y por lo tanto, no se puede pasar.

    Hay que destacar que el eje de coordenadas esta invertido respecto
    al usual, es decir:

    O-------------------> ordenadas (y)
    |
    |
    |
    |
    |
    V
    abscisas (x)

    :param mapa: lista de listas
    :return diccionario conteniendo las casillas en forma de tupla (x, y) y sus adyacentes
    """
    # Comprobación para que el mapa tenga 2 dimensiones
    if type(mapa[0]) not in (list, tuple):
        raise MapaMalFormado("El mapa introducido esta mal formado.")

    height = len(mapa)
    width = len(mapa[0]) if height else 0

    # Inicializa el grafo como diccionario
    graph = {(i, j): [] for i in range(height) for j in range(width) if mapa[i][j]}

    # Rellena por cada casilla, sus casillas adyacentes
    for row, col in graph.keys():
        # Verticales
        if 0 < row + 1 < height - 1 and mapa[row + 1][col]:
            graph[(row, col)].append(((1, 0), (row + 1, col)))
            graph[(row + 1, col)].append(((-1, 0), (row, col)))

        # Horizontales
        if 0 < col + 1 < width and mapa[row][col + 1]:
            graph[(row, col)].append(((0, 1), (row, col + 1)))
            graph[(row, col + 1)].append(((0, -1), (row, col)))

        # Hacia la diagonal inferior derecha
        if 0 < row + 1 < height - 1 and 0 < col + 1 < width - 1 and mapa[row + 1][col + 1]:
            graph[(row, col)].append(((1, 1), (row + 1, col + 1)))
            graph[(row + 1, col + 1)].append(((-1, -1), (row, col)))

        # Hacia la diagonal superior derecha
        if 0 < row - 1 < height and 0 < col + 1 < width and mapa[row - 1][col + 1]:
            graph[(row, col)].append(((-1, 1), (row - 1, col + 1)))
            graph[(row - 1, col + 1)].append(((1, -1), (row, col)))

    return graph


def shortest_path(start, end, graph):
    """
    Adaptación de: http://bryukh.com/labyrinth-algorithms/

    Realiza una Búsqueda Anchura Primero desde un nodo dado hasta un nodo final
    en el grafo y devuelve la ruta. En caso de no existir, un ruta, devolverá None.

    :param start: nodo de inicio en forma de tupla (x, y)
    :param end: nodo final en forma de tupla (x, y)
    :param graph: grafo en el que buscar
    :return Ruta en caso de existir (devuelve los pasos) y la longitud, si no, devuelve None
    """
    # Comprobar que graph es un grafo
    if type(graph) != dict:
        raise MapaMalFormado("El grafo introducido no es un grafo.")

    queue = deque([([], start)])

    visited = set()
    while queue:
        path, current = queue.popleft()
        if current == end:
            return ([path], len(path)) if type(path) is not list else (path, len(path))
        if current in visited:
            continue
        visited.add(current)
        for direction, neighbour in graph[current]:
            queue.append((path + [direction], neighbour))

    return None, float('inf')


def orient2d(a, b, c):
    return (b[0] - a[0]) * (c[1] - a[1]) - (c[0] - a[0]) * (b[1] - a[1])


def colinear(a, b, c):
    return orient2d(a, b, c) == 0


def intersect(j, k):
    j1, j2 = j
    k1, k2 = k
    return (
            orient2d(k1, k2, j1) * orient2d(k1, k2, j2) < 0 and
            orient2d(j1, j2, k1) * orient2d(j1, j2, k2) < 0)


def _bias(p0, p1):
    if (p0[1] == p1[1] and p0[0] > p1[0]) or p0[1] > p1[1]:
        return 0
    else:
        return -1


def render(v0, v1, v2):
    if orient2d(v0, v1, v2) < 0:
        v0, v1 = v1, v0
    x0 = min(v0[0], v1[0], v2[0])
    x1 = max(v0[0], v1[0], v2[0])
    y0 = min(v0[1], v1[1], v2[1])
    y1 = max(v0[1], v1[1], v2[1])
    for y in xrange(y0, y1 + 1):
        for x in xrange(x0, x1 + 1):
            p = x, y
            w0 = orient2d(v1, v2, p) + _bias(v1, v2)
            w1 = orient2d(v2, v0, p) + _bias(v2, v0)
            w2 = orient2d(v0, v1, p) + _bias(v0, v1)
            if w0 >= 0 and w1 >= 0 and w2 >= 0:
                yield p
