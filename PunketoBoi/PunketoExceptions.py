#!/usr/bin/python3
# -*- coding: utf-8 -*-


class ErrorConexion(Exception):
    """Error al conectar el faro"""
    pass


class MapaMalFormado(Exception):
    pass
